const mongoose = require('mongoose');
//estableciendo el parametro de conexion
mongoose.connect('mongodb://localhost:27017/tienda'); //agrega lal uri seguido del nombre del proyecto 
const miconexion = mongoose.connection; //variable que lleva la coneccion

miconexion.on('connected',()=>{
    console.log('La conexion fue exitosa');
})
miconexion.on('error',()=>{
    console.log('Error de conexion');
})

module.exports = mongoose; //Exportar la constante que tiene todos los objetos de conexion

//se debe importar al servidor como una libreria