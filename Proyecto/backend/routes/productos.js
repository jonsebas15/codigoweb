const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')
const miesquema = mongoose.Schema
const esquemaProducto = new miesquema({
    //sin comillas
    id : String,    //{type: String, required:true,(no null requerido) trim:true,(limpiesa de datos) unique:true,(que sea unico) lowercase: false,(almacenar en mayuscula o minusculas) index:{unique:true} }
    nombre : String,
    descripcion : String,
    precio : Number,
    estado : Boolean
});
const modeloProducto = mongoose.model('productos', esquemaProducto)
//siempre al terminar se tiene que exportar
module.exports = router

//Peticion get para prueba
router.get('/prueba', (req, res) => {
    res.end("Prueba de ruta OK!")
})

//Cargar un producto http://localhost:5000/api/productos/cargar/
//Listar todos las productos
router.get('/listar', (req, res) => {
    modeloProducto.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

router.post('/agregar', (req, res) => {
    const nuevoProducto = new modeloProducto({
        id:req.body.id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        precio: req.body.precio,
        activo: req.body.activo        
    })
    nuevoProducto.save(function(err)
    {
        if(!err)
        {
            res.send("El producto fue agregado exitosamente!!!")
        }
        else
        {
            res.send(err)
        }
    })
})

//Cargar un producto http://localhost:5000/api/productos/cargar/1
router.get('/cargar/:id', (req, res) => {
    modeloProducto.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

router.post('/editar', (req, res) => {
    modeloProducto.findOneAndUpdate(
        {id:req.body.id}
        ,{
            id:req.body.id,
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            precio: req.body.precio,
            activo: req.body.activo
        },
        (err) =>
        {
            if(!err)
            {
                res.send("El producto se actualizó exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

router.post('/borrar', (req, res) => {
    modeloProducto.findOneAndDelete(
        {id:req.body.id},
        (err) =>
        {
            if(!err)
            {
                res.send("El producto se elimino exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})





