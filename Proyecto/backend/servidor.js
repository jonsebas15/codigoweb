//importo la libreria express
const express = require('express');
const app = express();
//importo la libreria de conexion de mongodb (creada)... lleva /. para saber que esta en la raiz, misma direccion
const miconexion = require('./conexion');

const rutaProductos = require('./routes/productos');
app.use('/miApp/productos', rutaProductos);

//biblioteca de body-parser
const bodyParser = require('body-parser'); //hace paraconvertirlo a json

//para activar el body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:"true"})); //permite a hacer el tema de los acentos y demas 
//llamar a la ruta

app.listen(5000,
    function(){
        console.log("servidor corriendo ok en el puerto 5000 - http://localhost:5000");
    }
);
app.get('/', (req, res) => {
    res.end("Servidor Backend OK!")
})
//una peticion a mi servidor 
/*app.get('/', function(req, res) {
    res.send('Servidor backend ok');
  });*/

